import axios from "axios"
import { createEffect, createStore } from "effector"

export const getArrivalFx = createEffect(async () => {
  try {
    const response = await axios.get(
      `http://46.174.48.185:9111/api/v1/flights/arrive`
    )
    return response.data.flights
  } catch (err) {
    throw "error"
  }
})

export const $arrival = createStore([]).on(
  getArrivalFx.doneData,
  (_, payload) => payload
)

export const getDepartureFx = createEffect(async () => {
  try {
    const response = await axios.get(
      `http://46.174.48.185:9111/api/v1/flights/departure`
    )
    return response.data.flights
  } catch (err) {
    throw "error"
  }
})

export const $departure = createStore([]).on(
  getDepartureFx.doneData,
  (_, payload) => payload
)
