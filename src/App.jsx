import logo from "./logo.svg"
import "./App.css"
import { useUnit } from "effector-react"
import { $arrival, $departure, getArrivalFx, getDepartureFx } from "./model"
import { useEffect } from "react"

function App() {
  useEffect(() => {
    getDepartureFx()
    getArrivalFx()
    const a = setInterval(() => {
      getDepartureFx()
      getArrivalFx()
    }, 30000)
    return () => {
      clearInterval(a)
    }
  }, [])
  const arrival = useUnit($arrival)
  const departure = useUnit($departure)
  return (
    <div className="wrapper">
      <div className="container departure">
        <h2>Вылет</h2>
        {departure.map((el) => (
          <div key={el.id} className="flight-item">
            <div className="time">
              {new Date(el.point_a_time)
                .toLocaleString("ru-RU", {
                  day: "numeric",
                  month: "numeric",
                  hour: "2-digit",
                  minute: "2-digit",
                  hour12: false,
                  year: "numeric",
                })
                .replace(",", "")}
            </div>
            <div className="city">{el.point_b_city}</div>
            <div className="status">{el.status}</div>
          </div>
        ))}
      </div>
      <div className="container arrival">
        <h2>Прилет</h2>
        {arrival.map((el) => (
          <div key={el.id} className="flight-item">
            <div className="time">
              {new Date(el.point_b_time)
                .toLocaleString("ru-RU", {
                  day: "numeric",
                  month: "numeric",
                  hour: "2-digit",
                  minute: "2-digit",
                  hour12: false,
                  year: "numeric",
                })
                .replace(",", "")}
            </div>
            <div className="city">{el.point_a_city}</div>
            <div className="status">{el.status}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default App
